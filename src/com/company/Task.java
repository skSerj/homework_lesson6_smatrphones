package com.company;

import com.company.data.Generator;
import com.company.model.Smartphone;
import com.company.model.Store;

import java.util.Scanner;

public class Task {
    public void run() {
        Store[] stores = Generator.generate();

        Scanner scan = new Scanner(System.in);
        System.out.println("Cматфон какого производителя желаете приобрести? (Iphone, Samsung, Xiaomi, Huawei): ");
        String desiredManufacturer = scan.nextLine();

        System.out.println("введите минимальную сумму, которую вы можете выделить на покупку смартфона: ");
        Integer minAmount = Integer.parseInt(scan.nextLine());

        System.out.println("введите максимальную сумму, которую вы можете выделить на покупку смартфона: ");
        Integer maxAmount = Integer.parseInt((scan.nextLine()));

        System.out.println("В каком магазине желаете приобрести устройство? (Comfy, Citrus или Mobilka): ");
        String desiredStore = scan.nextLine();

        for (Store x : stores) {
            if (x.hasNameStore(desiredStore)) {
//                x.findSmartphoneByManufacturer(desiredManufacturer);
//                x.findByIntervalPrice(minAmount,maxAmount,x.getName(),x.getAddress());
                x.finderByPrice(x.getName(),x.getAddress());
            }
        }
    }
}