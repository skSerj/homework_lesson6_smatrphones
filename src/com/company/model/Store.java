package com.company.model;

import com.company.data.Generator;

public class Store {
    private String name;
    private String address;
    private Smartphone[] smartphones;

    public Store(String name, String address, Smartphone[] smartphones) {
        this.name = name;
        this.address = address;
        this.smartphones = smartphones;
    }

    public boolean hasNameStore(String storeName) {
        return name.equalsIgnoreCase(storeName);
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public void findSmartphoneByManufacturer(String manufacturer) {
        for (Smartphone i : smartphones) {
            if (i.getManufacturer().equalsIgnoreCase(manufacturer)) {
                System.out.println(String.format("ваш сматфон: %s %s", manufacturer, i.getModel()));
            }
        }
    }

    public void findByIntervalPrice(Integer minAmount, Integer maxAmount, String storeName, String storeAddress) {
        for (Smartphone b : smartphones) {
            if (b.getPrice() < maxAmount && b.getPrice() > minAmount) {
                System.out.println((String.format("В указанном диапазоне цен представлены след. смартфоны: %s %s ", b.getManufacturer(), b.getModel())) + "в магазине: " + storeName + " , по адресу: " + storeAddress);
            }
        }
    }

    public void finderByPrice(String storeName, String storeAddress) {
        int minPriceSmartphoneInStore = smartphones[0].getPrice();
        for (Smartphone a : smartphones) {
            if (a.getPrice() < minPriceSmartphoneInStore) {
                minPriceSmartphoneInStore = a.getPrice();
                System.out.println("самый дешёвый смартфон в магазине: " + a.getManufacturer() + " " + a.getModel() + " по цене: " + a.getPrice() + ", адрес магазина: " + storeAddress);
            }
        }
    }
}